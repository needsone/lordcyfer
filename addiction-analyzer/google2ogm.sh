#!/bin/bash

json2csv -i watch-history.json -o watch-history.csv -f title,titleUrl,subtitles.[0].name,subtitles.[0].url,time
cat watch-history.csv | sed -e 's/""//g' > watch-history_parsed.csv
csv2sql -t history_watch -f watch-history_parsed.csv
sqlite3 db_watch.db < SQL-watch-history_parsed.sql
