# The target is too do a tool to see simply your data deliver from google & co.

## Tools

### json2csv

The files are json files.

Exemple from google youtube watch history :

```
{
  "header": "YouTube",
  "title": "Vous avez regardé Crowd laughs as Gaetz is called out for his OWN DUI after hitting Hunter Biden for substance abuse",
  "titleUrl": "https://www.youtube.com/watch?v=NidVfdQk_So",
  "subtitles": [{
    "name": "Brian Tyler Cohen",
    "url": "https://www.youtube.com/channel/UCQANb2YPwAtK-IQJrLaaUFw"
  }],
  "time": "2019-12-14T08:19:32",
  "products": ["YouTube"]
},
```

#  Display your YouTube history

In this project we will from the json files given by Google for youtube.
Create a CSV and a sqlite db.
With the videos name and url, channel name and url, time

# Process to get data

Request to Google your Data

Get all the files in json format use the	watch-history.json

json2csv -i watch-history.json -o watch-history.csv -f title,titleUrl,subtitles.[0].name,subtitles.[0].url,time
cat  watch-history.csv | sed -e 's/""//g' > watch-history_parsed.csv
csv2sql -t history_watch -f watch-history_parsed.csv


## and now fill a sqlite db

$ sqlite3 db_watch.db < SQL-watch-history_parsed.sql

You have the CSV : watch-history_parsed.csv<br/>
The SQL (for sqlite) : SQL-watch-history_parsed.sql<br/>
and you can use : sqlite3 db_watch.db to play with your historic of Youtube.<br/>
